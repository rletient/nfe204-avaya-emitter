package org.homework.nfe204

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.StringSerializer

import java.util.concurrent.Future

// -f  /home/developper/dev/nfe204-project/nfe204-dataset-exploder/telephonie.txt -t telephonieEvent -k 172.18.0.3:9092,172.18.0.4:9092,172.18.0.5:9092
class AvayaEmitter {


    private File fileToEmit
    private String topic

    private KafkaProducer<String,String> producer

    private AvayaEmitter(){}


    void emitWithoutKey() {
        int index = 0
        fileToEmit.eachLine { String line ->
            ProducerRecord<Long, String> record = new ProducerRecord<>(topic, line)
            Future<RecordMetadata> fRecordMedatadata = producer.send(record)
            RecordMetadata recordMetadata = fRecordMedatadata.get()
            println("Message bien envoyé : Partition(${recordMetadata.partition()}) - Offset(${recordMetadata.offset()}")
            sleep(1000)
        }
    }


    static void main(String[] args) {
        def cli = new CliBuilder(usage: 'java -jar AvayaEmitter -[fkt]')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            f longOpt: 'file', required: true, args: 1, argName: 'file', 'Fichier d evenement à émettre'
            k longOpt: 'kafkaBrokers', required: true, args: 1, argName: 'kafkaBrokers', 'Liste des urls des noeuds Kafka'
            t longOpt: 'topic', required: true, args: 1, argName: 'topic', 'Nom du topic cible'
            a longOpt: 'ack', required: true, args: 1, argName: 'ack', 'Nb Ackknowledeg 0,1 or all'
        }
        def options = cli.parse(args)
        if (!options) {
            return
        }
        if (options.h) {
            cli.usage()
        }
        Properties properties = new Properties()
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, options.k)
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "AvayaEmitter")
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName())
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName())
        properties.put(ProducerConfig.ACKS_CONFIG, options.a)

        AvayaEmitter emitter = new AvayaEmitter()
        emitter.fileToEmit = new File(options.f)
        emitter.topic = options.t
        emitter.producer = new KafkaProducer<>(properties)
        emitter.emitWithoutKey()
        println("Fin de production")
    }
}
